Git是分散式版本控制軟體，可說是軟體開發、更新、維護等很重要的一個工具！若要開發大眾使用、並需定期更新的軟體，那就需要做版本控制的動作，學習Git便能幫助軟體的更新，使大眾能同步所使用的軟體！此外，Git可讓許多開發者共同開發專案(軟體、系統等)，集思廣益，將可使專案開發速度變快，也能幫助專案維護一定的品質！
參考網址：https://zh.wikipedia.org/wiki/Git

Gitlab：基於 Git 的完全整合之軟體開發平台
Github：透過Git進行版本控制的軟體原始碼代管服務平台
參考網址：https://zh.wikipedia.org/wiki/GitLab、https://zh.wikipedia.org/wiki/GitHub

CI/CD
CI：Continuous Integration (持續整合)，能自動化檢測專案開發的程式碼！
CD：Continuous Deployment (持續部署)，經過CI檢測，若通過，則進行CD，自動將程式碼傳到伺服器，並架設好web server，開啟服務等。
參考網址：https://blog.kennycoder.io/2020/04/07/CI-CD-%E6%8C%81%E7%BA%8C%E6%80%A7%E6%95%B4%E5%90%88-%E9%83%A8%E7%BD%B2-%E5%9B%A0%E7%82%BA%E6%87%B6%EF%BC%8C%E6%89%80%E4%BB%A5%E6%9B%B4%E8%A6%81CI-CD%EF%BC%81%E6%A6%82%E5%BF%B5%E8%AC%9B%E8%A7%A3%EF%BC%81/


Git語法練習之問題小記
1-1. Q：git -C <path>這語法的作用是甚麼?
     A：指定一個路徑為庫 如果沒有這路徑，會直接創建。

1-2. Q：使用 git -C <path> 這語法時，是否要把<>打出來?
     A：不用，<>這裡面放的是輸入此語法，需放入(甚麼樣種類)的內容。


其他讀過的網站：
     1.https://blog.techbridge.cc/2018/01/17/learning-programming-and-coding-with-python-git-and-github-tutorial/
     2.https://rommelhong.medium.com/%E4%B8%83%E5%88%86%E9%90%98%E5%AD%B8%E6%9C%83gitlab-ecdcbcb42b9c
     3.https://gitbook.tw/chapters/github/pull-from-github.html
     4.https://tedsieblog.wordpress.com/2017/01/19/basic-git-commands/


目前暫時寫到這邊，若有錯誤，還請大家多多指教！
